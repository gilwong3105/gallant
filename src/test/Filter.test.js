import React from 'react';
import { render } from '@testing-library/react';
import Filter from '../components/Filter';

const breeds = [
  { label: 'Husky', value: 'husky' },
  { label: 'Corgi', value: 'corgi' },
];

const handleSubmit = jest.fn();
const handleClear = jest.fn();

test('renders without crashing', () => {
  const { unmount } = render(
    <Filter
      breeds={breeds}
      handleSubmit={handleSubmit}
      handleClear={handleClear}
      showClearButton={true}
    />
  );
  unmount();
});


