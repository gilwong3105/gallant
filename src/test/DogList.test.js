import React from 'react';
import { render } from '@testing-library/react';
import DogList from '../components/DogList';

test('should render seelcted dogs from filter', () => {
  const selectedDogs = {
    Husky: ['https://images.dog.ceo/breeds/husky/20180901_150234.jpg'],
  };

  const { queryByText } = render(<DogList dogs={selectedDogs} />);
  expect(queryByText('Corgi')).toBeNull();
  expect(queryByText('Husky')).not.toBeNull();
  expect(document.querySelectorAll('img')).toHaveLength(1);
});
