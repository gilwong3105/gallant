import React, { useState } from 'react';
import useDogBreeds from './hooks/useDogBreeds';
import Filter from './components/Filter';
import DogList from './components/DogList';
import axios from 'axios';
import './App.css';
import './Filter.css';

const BASE_IMAGE_URL = `https://dog.ceo/api/breed`;

function App() {
  const [dogs, setDogs] = useState();
  const { dogBreeds } = useDogBreeds();
  const showClearbutton = !!dogs;

  const handleSubmit = async selectedBreeds => {
    if (!selectedBreeds || selectedBreeds.length === 0) {
      return setDogs();
    }

    let map = {};

    for (let breed of selectedBreeds) {
      if (dogs && dogs[breed.label]) {
        map[breed.label] = dogs[breed.label];
      } else {
        const { data } = await axios.get(
          `${BASE_IMAGE_URL}/${breed.value}/images`
				);
				
        map[breed.label] = data.message;
      }
    }
    setDogs(map);
  };

  const handleClear = () => setDogs(null);

  return (
    <div className='App'>
      <h1>Dog Finder</h1>
      <Filter
        breeds={dogBreeds}
        handleSubmit={handleSubmit}
        handleClear={handleClear}
        showClearButton={showClearbutton}
      />
      {dogs && <DogList dogs={dogs} />}
    </div>
  );
}

export default App;
