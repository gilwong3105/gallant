import React from 'react';

const Dog = ({ dog, index }) => (
  <li>
    <img src={dog} alt={index} />
  </li>
);

export default Dog;
