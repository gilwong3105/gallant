import React from 'react';
import Dog from './Dog';

const DogList = ({ dogs }) => {
  return (
    <div className='container'>
      {dogs
        ? Object.keys(dogs).map(key => {
            return (
              <div key={key}>
                <h1>{key}</h1>
                <ul className='list-container'>
                  {dogs[key].map((dog, i) => (
                    <Dog key={key + i} dog={dog} index={i} />
                  ))}
                </ul>
              </div>
            );
          })
        : null}
    </div>
  );
};

export default DogList;
