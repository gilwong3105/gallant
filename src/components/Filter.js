import React, { useState } from 'react';
import Select from 'react-select';

export const Filter = ({ breeds, handleSubmit }) => {
  const [selectedBreeds, setSelectedBreeds] = useState([]);

  const handleSelect = breed => {
    setSelectedBreeds(breed);
    handleSubmit(breed);
  };

  return (
    <div className='filter-container'>
      {/* I was initially building my own select however I had issues with add checkboxes so i used a lib instead */}

      {/* <select  className='select-breed' onChange={handleSelect}>
        <option selected={selectedBreeds.length === 0}>
          Select a breed
        </option>
        {breeds.map(breed => (
          <option key={breed}>{breed}</option>
        ))}
      </select> */}

      <Select
        className='select-breed'
        value={selectedBreeds}
        isMulti={true}
        onChange={handleSelect}
        options={breeds}
        placeholder='Select a breed'
      />
    </div>
  );
};

export default Filter;
