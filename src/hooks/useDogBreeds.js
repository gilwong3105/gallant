import { useEffect, useState } from 'react';
import axios from 'axios';

export default function useDogBreeds() {
  const [dogBreeds, setDogBreeds] = useState(
    JSON.parse(localStorage.getItem('breeds'))
  );

  const setLocalStorage = breeds =>
    localStorage.setItem('breeds', JSON.stringify(breeds));

  useEffect(() => {
    const getDogsBreeds = async () => {
      const { data } = await axios.get('https://dog.ceo/api/breeds/list/all');
      setDogBreeds(data.message);
      setLocalStorage(data.message);
    };

    if (!dogBreeds) {
      getDogsBreeds();
    }
  }, [dogBreeds]);

  return {
    dogBreeds:
      Object.keys(dogBreeds).map(breed => ({
        label: breed.charAt(0).toUpperCase() + breed.slice(1),
        value: breed,
      })) || [],
  };
}
